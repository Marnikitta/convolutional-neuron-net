package org.tomat.spbsu.mynn;

import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        new Main().run();
    }

    public void run() {
        ArrayList<Pair> dataset = new ArrayList<>();
        dataset.add(new Pair(new double[][]{{0, 1}}, new double[]{-1, 1}));
        dataset.add(new Pair(new double[][]{{1, 1}}, new double[]{1, -1}));
        dataset.add(new Pair(new double[][]{{1, 0}}, new double[]{-1, 1}));
        dataset.add(new Pair(new double[][]{{0, 0}}, new double[]{1, -1}));
        XORNet ner = new XORNet();
        Random rd = new Random();
        for (int i = 0; i < 100000; ++i) {
            Pair data = dataset.get(Math.abs(rd.nextInt()) % 4);
            ner.learnCycle(data.data, data.resut);
            if (i % 100 == 0) {
                System.out.println("------");
                System.out.println(ner.evaluate(dataset.get(0).data)[0]);
                System.out.println(ner.evaluate(dataset.get(1).data)[0]);
                System.out.println(ner.evaluate(dataset.get(2).data)[0]);
                System.out.println(ner.evaluate(dataset.get(3).data)[0]);
            }
        }
    }

    public class Pair {
        private double[][] data;
        private double[] resut;

        public double[][] getData() {
            return data;
        }

        public void setData(double[][] data) {
            this.data = data;
        }

        public double[] getResut() {
            return resut;
        }

        public void setResut(double[] resut) {
            this.resut = resut;
        }

        public Pair(double[][] data, double[] resut) {
            this.data = data;
            this.resut = resut;
        }
    }
}
