package org.tomat.spbsu.mynn;

import org.tomat.spbsu.nn.core.NeuralNetwork;
import org.tomat.spbsu.nn.core.Weight;
import org.tomat.spbsu.nn.nnet.elements.layers.FullyConnectedLayer;
import org.tomat.spbsu.nn.nnet.elements.layers.OutputLayer;
import org.tomat.spbsu.nn.nnet.functions.NNetFunctions;

public class XORNet extends NeuralNetwork {
    public XORNet() {
        super(2, 1);
        FullyConnectedLayer f1 = new FullyConnectedLayer(getInputLayer(), 2, NNetFunctions.tanh());
        FullyConnectedLayer f2 = new FullyConnectedLayer(f1, 2, NNetFunctions.tanh());
        OutputLayer out = new OutputLayer(f2);
        addLayer(f1);
        addLayer(f2);
        addLayer(out);
        setOutputLayer(out);
        setWeights(Weight.list);
        setA(0.3);
        setN(0.2);
    }
}
