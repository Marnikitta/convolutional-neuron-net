package org.tomat.spbsu.nn.core;

import org.tomat.spbsu.nn.nnet.functions.ActivationFunction;
import org.tomat.spbsu.nn.nnet.functions.LocalFieldFunction;
import org.tomat.spbsu.nn.nnet.functions.NNetFunctions;

import java.util.ArrayList;

public class Neuron {
    private static int counter = 0;
    private int number = counter++;

    private double delta = 0;
    private double localField = 0;
    private double result = 0;

    private ArrayList<Connection> inConnections = new ArrayList<>();
    private ArrayList<Connection> outConnections = new ArrayList<>();
    private ActivationFunction activationFunction = NNetFunctions.tanh();
    private LocalFieldFunction localFieldFunction = NNetFunctions.sum();

    private String label = Integer.toString(number);

    public Neuron() {
    }


    public Neuron(ActivationFunction activator) {
        this.activationFunction = activator;
    }

    public Neuron(ActivationFunction activationFunction, LocalFieldFunction localFieldFunction) {
        this.activationFunction = activationFunction;
        this.localFieldFunction = localFieldFunction;
    }

    public double evaluate() {

        if (inConnections.size() == 0) {
            return result;
        }
        Double[] input = new Double[inConnections.size()];
        for (int i = 0; i < inConnections.size(); ++i) {
            input[i] = inConnections.get(i).getWeightedValue();
        }

        localField = localFieldFunction.apply(input);
        result = activationFunction.apply(localField);

        return result;
    }

    public double evaluateDelta() {
        if (outConnections.size() == 0) {
            return delta;
        }
        double sum = 0;
        for (Connection c : outConnections) {
            sum += c.getWeightedDelta();
        }
        delta = activationFunction.derivative().apply(result) * sum;
        for (Connection connection : inConnections) {
            connection.getWeight().deltaModify(delta);
        }
        return delta;
    }

    public void addBias(Weight w) {
        Neuron biasNeuron = new Neuron(null);
        biasNeuron.setLabel("BIAS");
        biasNeuron.setResult(1);
        new Connection(biasNeuron, this, w);
    }

    public void setResult(double result) {
        this.result = result;
    }

    public void addInConnection(Connection connection) {
        inConnections.add(connection);
    }

    public void addOutConnection(Connection connection) {
        outConnections.add(connection);
    }

    public double getLocalField() {
        return localField;
    }

    public double getResult() {
        return result;
    }

    public double getDelta() {
        return delta;
    }

    public void setDelta(double delta) {
        this.delta = delta;
    }

    public void setLabel(String label) {

        this.label = label;
    }

    public String toString() {
        return label;
    }
}
