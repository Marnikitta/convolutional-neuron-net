package org.tomat.spbsu.nn.core;

import java.util.ArrayList;

public class Connection {
    private Weight weight;
    public static ArrayList<Connection> list = new ArrayList<>();

    private Neuron inNeuron;
    private Neuron outNeuron;

    public Connection(Neuron inNeuron, Neuron outNeuron, Weight weight) {
        this.weight = weight;
        this.inNeuron = inNeuron;
        this.outNeuron = outNeuron;
        outNeuron.addInConnection(this);
        inNeuron.addOutConnection(this);
        list.add(this);
    }

    public double getValue() {
        return inNeuron.getResult();
    }

    public double getWeightedDelta() {
        return weight.getValue() * outNeuron.getDelta();
    }

    public Weight getWeight() {
        return weight;
    }

    public double getWeightedValue() {
        weight.setInResult(getValue());
        return weight.getValue() * inNeuron.getResult();
    }

    public String toString() {
        return "[" + weight + ":" + inNeuron + "->" + outNeuron + "]";
    }
}
