package org.tomat.spbsu.nn.core;

import java.util.function.Consumer;

public interface Layer {
    void evaluate();

    void evaluateDelta();

    void fixWeights();

    void forEach(Consumer<Neuron> consumer);

    int neuronsSize();
}
