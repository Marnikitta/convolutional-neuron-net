package org.tomat.spbsu.nn.core;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.DoubleSupplier;

public class Weight {
    public static ArrayList<Weight> list = new ArrayList<>();

    public static int prob = 2;

    protected double value = 0;
    protected double previousDelta = 0;

    protected DoubleSupplier suplier;
    protected double delta;


    protected double inResult;
    protected Random rd = new Random();

    public void update(double alpha, double mu) {
        if (rd.nextInt() % prob != 0) {
            previousDelta = alpha * previousDelta + mu * delta * inResult;
            value = value + previousDelta;
        }
    }

    public Weight(final double value) {
        this.value = value;
        suplier = () -> value;
        list.add(this);
    }

    public Weight(final DoubleSupplier suplier) {
        this.suplier = suplier;
        randomize();
        list.add(this);
    }

    public void deltaModify(double delta) {
        this.delta = delta;
    }

    public void randomize() {
        value = suplier.getAsDouble();
    }

    public void setInResult(double inResult) {
        this.inResult = inResult;
    }

    public double getValue() {
        return value;
    }

    public void setSuplier(final DoubleSupplier suplier) {
        this.suplier = suplier;
    }

    public String toString() {
        return value + " ";
    }
}
