package org.tomat.spbsu.nn.core;

import org.tomat.spbsu.nn.nnet.elements.layers.InputLayer2D;
import org.tomat.spbsu.nn.nnet.elements.layers.Layer2D;
import org.tomat.spbsu.nn.nnet.elements.layers.OutputLayer;

import java.util.ArrayList;

public abstract class NeuralNetwork {
    private ArrayList<Layer> layers = new ArrayList<>();


    private ArrayList<Weight> weights = new ArrayList<>();
    protected InputLayer2D inputLayer;
    private OutputLayer outputLayer;
    private double n;
    private double a;
    private ArrayList<Connection> c = Connection.list;

    public NeuralNetwork(int inputWidth, int inputHeigt) {
        inputLayer = new InputLayer2D(inputWidth, inputHeigt);
        layers.add(inputLayer);
    }

    public double[] evaluate(double[][] input) {
        inputLayer.setInputValues(input);
        for (Layer l : layers) {
            l.evaluate();
        }
        return outputLayer.getResult();
    }

    private void backPropagate(double[] ideal) {
        outputLayer.evaluateDelta(ideal);
        for (int i = layers.size() - 1; i >= 0; --i) {
            layers.get(i).evaluateDelta();
        }
    }

    private void updateWeights() {
        for (Weight w : weights) {
            w.update(a, n);
        }
    }

    public double getSE() {
        return outputLayer.getSE();
    }

    public void learnCycle(double[][] input, double[] output) {
        evaluate(input);
        backPropagate(output);
        updateWeights();
    }

    protected void setWeights(ArrayList<Weight> weights) {
        this.weights = weights;
    }

    protected void addLayer(Layer l) {
        layers.add(l);
    }

    protected Layer2D getInputLayer() {
        return inputLayer;
    }

    protected void setOutputLayer(OutputLayer outputLayer) {
        this.outputLayer = outputLayer;
    }

    public void setN(double n) {
        this.n = n;
    }

    public void setA(double a) {
        this.a = a;
    }

    public String weightsToString() {
        return weights.toString();
    }
}

