package org.tomat.spbsu.nn.nnet.elements;

import org.tomat.spbsu.nn.core.Weight;

import java.util.ArrayList;
import java.util.function.DoubleSupplier;

public class SharedWeight extends Weight {
    ArrayList<Double> deltas = new ArrayList<>();

    public SharedWeight(DoubleSupplier suplier) {
        super(suplier);
    }

    @Override
    public void update(double alpha, double mu) {
        delta = 0;
        for (double d : deltas) {
            delta += d;
        }
        delta /= deltas.size();
        update(alpha, mu);
        deltas.clear();
    }

    @Override
    public void deltaModify(double delta) {
        deltas.add(delta);
    }
}
