package org.tomat.spbsu.nn.nnet.functions;

import java.util.function.Function;

public interface LocalFieldFunction extends Function<Double[], Double> {
}
