package org.tomat.spbsu.nn.nnet.elements.layers;

import org.tomat.spbsu.nn.core.Layer;
import org.tomat.spbsu.nn.core.Neuron;
import org.tomat.spbsu.nn.nnet.elements.FeatureMap;

import java.util.Arrays;
import java.util.function.Consumer;

public abstract class Layer2D implements Layer {
    protected FeatureMap[] featureMaps;

    public Layer2D() {
    }

    protected Layer2D(int size) {
        featureMaps = new FeatureMap[size];
    }

    @Override
    public void evaluate() {
        forEach(x -> x.evaluate());
    }

    @Override
    public void evaluateDelta() {
        forEach(n -> n.evaluateDelta());
    }

    @Override
    public void fixWeights() {
    }

    @Override
    public void forEach(Consumer<Neuron> consumer) {
        for (FeatureMap fm : featureMaps) {
            fm.forEach(consumer);
        }
    }

    public FeatureMap[] getFeatureMaps() {
        return Arrays.copyOf(featureMaps, featureMaps.length);
    }

    public int getSize() {
        return featureMaps.length;
    }

    public String toString() {
        return Arrays.toString(featureMaps);
    }

    @Override
    public int neuronsSize() {
        int size = 0;
        for (FeatureMap fm : featureMaps) {
            size += fm.getHeight() * fm.getHeight();
        }
        return size;
    }
}
