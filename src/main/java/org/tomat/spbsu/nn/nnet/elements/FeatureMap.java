package org.tomat.spbsu.nn.nnet.elements;

import org.tomat.spbsu.nn.core.Connection;
import org.tomat.spbsu.nn.core.Neuron;
import org.tomat.spbsu.nn.core.Weight;
import org.tomat.spbsu.nn.nnet.functions.ActivationFunction;
import org.tomat.spbsu.nn.nnet.functions.NNetFunctions;

import java.util.Arrays;
import java.util.function.Consumer;

public class FeatureMap {
    private static int counter = 0;
    private int number = counter++;

    private Neuron[][] neurons;
    private final int width;
    private final int height;

    private String label = number + "";


    public FeatureMap(int width, int height) {
        neurons = new Neuron[height][width];
        this.width = width;
        this.height = height;
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                this.neurons[i][j] = new Neuron();
            }
        }
    }

    public FeatureMap(FeatureMap parentMap, int frameWidth, int frameHeight, Weight[][] kernel, Weight bias, ActivationFunction activator) {
        if (parentMap == null) {
            throw new IllegalArgumentException("parentMap is null");
        }
        if (parentMap.height - frameHeight < 0 || parentMap.width - frameWidth < 0) {
            throw new IllegalArgumentException("Frame sizes larger then parentMap's");
        }

        this.width = parentMap.height - frameHeight + 1;
        this.height = parentMap.width - frameWidth + 1;
        neurons = new Neuron[this.height][this.width];


        for (int i = 0; i <= parentMap.height - frameHeight; ++i) {
            for (int j = 0; j <= parentMap.width - frameWidth; ++j) {
                neurons[i][j] = parentMap.getSubMap(i, j, frameWidth, frameHeight).convolve(kernel, activator);
                neurons[i][j].addBias(bias);
            }
        }
    }

    public FeatureMap getSubMap(int x, int y, int width, int height) {
        if (x + width > this.width || y + height > this.height)
            throw new IllegalArgumentException("Out of range");

        FeatureMap result = new FeatureMap(width, height);
        for (int i = 0; i < height; ++i) {
            System.arraycopy(neurons[i + y], x, result.neurons[i], 0, width);
        }
        return result;
    }

    public Neuron convolve(Weight[][] weights) {
        return convolve(weights, NNetFunctions.sigmoid());
    }

    public Neuron convolve(Weight[][] weights, ActivationFunction activator) {
        if (weights == null || this.height > weights.length || this.width > weights[0].length) {

            throw new IllegalArgumentException("Weight matrix is wrong size");
        }
        Neuron result = new Neuron(activator, NNetFunctions.sum());
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                new Connection(neurons[i][j], result, weights[i][j]);
            }
        }
        return result;
    }

    public void forEach(Consumer<Neuron> consumer) {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                consumer.accept(neurons[i][j]);
            }
        }
    }

    public Neuron[][] getNeurons() {
        return neurons;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getLabel() {
        return label;
    }

    public String toString() {
        return Arrays.deepToString(neurons);
    }
}
