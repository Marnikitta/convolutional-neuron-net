package org.tomat.spbsu.nn.nnet.elements.layers;

import org.tomat.spbsu.nn.core.Connection;
import org.tomat.spbsu.nn.core.Layer;
import org.tomat.spbsu.nn.core.Neuron;
import org.tomat.spbsu.nn.core.Weight;
import org.tomat.spbsu.nn.nnet.functions.ActivationFunction;
import org.tomat.spbsu.nn.util.Weights;

public class FullyConnectedLayer extends LinearLayer implements Layer {

    public FullyConnectedLayer(Layer parentLayer, ActivationFunction activationFunction) {
        this(parentLayer, parentLayer.neuronsSize(), activationFunction);
    }

    public FullyConnectedLayer(Layer parentLayer, int size, ActivationFunction activationFunction) {
        neurons = new Neuron[size];
        for (int i = 0; i < size; ++i) {
            Neuron result = new Neuron(activationFunction);
            parentLayer.forEach(n -> {
                new Connection(n, result, new Weight(Weights.randomSupplier((double) 1 / size)));
            });
            neurons[i] = result;
            neurons[i].addBias(new Weight(Weights.randomSupplier((double) 1 / size)));
        }
    }
}
