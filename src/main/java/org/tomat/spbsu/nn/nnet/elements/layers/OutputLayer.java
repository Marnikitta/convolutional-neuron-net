package org.tomat.spbsu.nn.nnet.elements.layers;

import org.tomat.spbsu.nn.core.Connection;
import org.tomat.spbsu.nn.core.Layer;
import org.tomat.spbsu.nn.core.Neuron;
import org.tomat.spbsu.nn.core.Weight;
import org.tomat.spbsu.nn.nnet.functions.NNetFunctions;

import java.util.ArrayList;

public class OutputLayer extends LinearLayer {

    public OutputLayer(Layer parentLayer) {
        ArrayList<Neuron> neurons1 = new ArrayList<>();
        parentLayer.forEach(n -> {
            Neuron localN = new Neuron(NNetFunctions.linear(), NNetFunctions.sum());
            neurons1.add(localN);
            new Connection(n, localN, new Weight(1));
        });
        neurons = new Neuron[neurons1.size()];
        for (int i = 0; i < neurons.length; ++i) {
            neurons[i] = neurons1.get(i);
        }
    }

    public void evaluateDelta(double[] ideal) {
        for (int i = 0; i < neuronsSize(); ++i) {
            neurons[i].setDelta(ideal[i] - getResult()[i]);
        }
    }

    public double getSE() {
        double result = 0;
        for (Neuron n : neurons) {
            result += Math.pow(n.getDelta(), 2);
        }
        return result;
    }
}
