package org.tomat.spbsu.nn.nnet.elements.layers;

import org.tomat.spbsu.nn.nnet.elements.FeatureMap;
import org.tomat.spbsu.nn.nnet.elements.SharedWeight;
import org.tomat.spbsu.nn.nnet.functions.NNetFunctions;
import org.tomat.spbsu.nn.util.Weights;

import java.util.ArrayList;

public class ConvolutionalLayer extends Layer2D {
    private ArrayList<SharedWeight[][]> kernels = new ArrayList<>();

    public ConvolutionalLayer(Layer2D parentLayer, int frameWidth, int framwHeight, int number) {
        int size = number * parentLayer.getSize();
        featureMaps = new FeatureMap[size];
        int p = 0;
        for (int i = 0; i < number; ++i) {
            SharedWeight[][] kernel = Weights.generateSharedWeights(frameWidth, framwHeight);
            SharedWeight biasWeight = Weights.generateBiasWeight(frameWidth, framwHeight);
            kernels.add(kernel);
            for (FeatureMap fm : parentLayer.featureMaps) {
                featureMaps[p++] = new FeatureMap(fm, frameWidth, frameWidth, kernel, biasWeight, NNetFunctions.linear());
            }
        }
    }
}
