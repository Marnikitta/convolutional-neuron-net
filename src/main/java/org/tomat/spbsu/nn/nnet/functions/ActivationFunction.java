package org.tomat.spbsu.nn.nnet.functions;

import java.util.function.DoubleFunction;

public interface ActivationFunction extends DoubleFunction<Double> {
    public DoubleFunction<Double> derivative();
}
