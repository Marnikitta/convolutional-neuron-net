package org.tomat.spbsu.nn.nnet.elements.layers;

import org.tomat.spbsu.nn.core.Layer;
import org.tomat.spbsu.nn.core.Neuron;

import java.util.Arrays;
import java.util.function.Consumer;

public class LinearLayer implements Layer {
    protected Neuron[] neurons;

    @Override
    public void evaluate() {
        forEach(n -> n.evaluate());
    }

    @Override
    public void evaluateDelta() {
        forEach(n -> n.evaluateDelta());
    }

    @Override
    public void fixWeights() {

    }

    @Override
    public void forEach(Consumer<Neuron> consumer) {
        for (Neuron n : neurons) {
            consumer.accept(n);
        }
    }

    public double[] getResult() {
        double[] result = new double[neurons.length];
        for (int i = 0; i < result.length; ++i) {
            result[i] = neurons[i].getResult();
        }
        return result;
    }

    @Override
    public int neuronsSize() {
        return neurons.length;
    }

    public String toString() {
        return Arrays.toString(neurons);
    }

}
