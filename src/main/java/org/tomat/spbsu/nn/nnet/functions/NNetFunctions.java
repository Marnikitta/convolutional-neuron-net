package org.tomat.spbsu.nn.nnet.functions;


import java.util.function.DoubleFunction;

public final class NNetFunctions {

    public static ActivationFunction sigmoid() {
        return new Sigmoid(1);
    }

    public static ActivationFunction sigmoid(double a) {
        return new Sigmoid(a);
    }

    public static ActivationFunction tanh() {
        return new Tanh(1.7159, 2D / 3);
    }

    public static ActivationFunction tanh(double a, double b) {
        return new Tanh(a, b);
    }

    public static ActivationFunction linear() {
        return new Linear();
    }

    private static class Sigmoid implements ActivationFunction {
        private double a;

        public Sigmoid(double a) {
            this.a = a;
        }

        @Override
        public DoubleFunction<Double> derivative() {
            return x -> a * x * (1 - x);
        }

        @Override
        public Double apply(double value) {
            return (double) 1 / (1D + Math.exp(-a * value));
        }
    }

    private static class Tanh implements ActivationFunction {
        private double a;
        private double b;

        public Tanh(double a, double b) {
            this.a = a;
            this.b = b;
        }

        @Override
        public DoubleFunction<Double> derivative() {
            return (x) -> (b / a) * (a - x) * (a + x);
        }

        @Override
        public Double apply(double value) {
            return a * Math.tanh(b * value);
        }
    }

    private static class Linear implements ActivationFunction {

        @Override
        public DoubleFunction<Double> derivative() {
            return (x) -> 1d;
        }

        @Override
        public Double apply(double value) {
            return value;
        }
    }

    public static LocalFieldFunction max() {
        return new Max();
    }

    public static LocalFieldFunction sum() {
        return new Sum();
    }

    private static class Max implements LocalFieldFunction {

        @Override
        public Double apply(Double[] doubles) {
            if (doubles == null) {
                throw new NullPointerException();
            }
            double result = Double.MIN_VALUE;
            for (double d : doubles) {
                result = Math.max(result, d);
            }
            return result;
        }
    }

    private static class Sum implements LocalFieldFunction {

        @Override
        public Double apply(Double[] doubles) {
            double result = 0;
            for (double d : doubles) {
                result += d;
            }
            return result;
        }
    }
}
