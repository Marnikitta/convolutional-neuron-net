package org.tomat.spbsu.nn.nnet.elements.layers;

import org.tomat.spbsu.nn.core.Connection;
import org.tomat.spbsu.nn.core.Neuron;
import org.tomat.spbsu.nn.core.Weight;
import org.tomat.spbsu.nn.nnet.elements.FeatureMap;
import org.tomat.spbsu.nn.nnet.functions.NNetFunctions;

public class PoolingLayer extends Layer2D {
    public PoolingLayer(Layer2D parent, int compressFactor) {
        super(parent.getSize());
        for (int i = 0; i < parent.featureMaps.length; ++i) {
            FeatureMap currentParentMap = parent.featureMaps[i];
            int newWidth = currentParentMap.getWidth() / compressFactor;
            int newHeight = currentParentMap.getHeight() / compressFactor;
            featureMaps[i] = new FeatureMap(newWidth, newHeight);
            for (int y = 0; y < newHeight; ++y) {
                for (int x = 0; x < newWidth; ++x) {
                    Neuron result = new Neuron(NNetFunctions.tanh(), NNetFunctions.max());
                    int inParentX = x * compressFactor;
                    int inParentY = y * compressFactor;
                    for (int py = inParentY; py < inParentY + compressFactor; ++py) {
                        for (int px = inParentX; px < inParentX + compressFactor; ++px) {
                            if (currentParentMap.getWidth() <= px || currentParentMap.getHeight() <= py) {
                                throw new IllegalStateException();
                            }
                            result.addInConnection(new Connection(currentParentMap.getNeurons()[py][px], result, new Weight(1)));
                        }
                    }
                    featureMaps[i].getNeurons()[y][x] = result;
                }
            }
        }
    }
}
