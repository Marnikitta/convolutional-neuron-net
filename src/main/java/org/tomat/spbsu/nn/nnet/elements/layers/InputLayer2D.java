package org.tomat.spbsu.nn.nnet.elements.layers;

import org.tomat.spbsu.nn.nnet.elements.FeatureMap;

public class InputLayer2D extends Layer2D {
    public InputLayer2D(int width, int height) {
        featureMaps = new FeatureMap[1];
        featureMaps[0] = new FeatureMap(width, height);
    }

    public void setInputValues(double[][] values) {
        FeatureMap fm = featureMaps[0];
        for (int i = 0; i < fm.getHeight(); ++i) {
            for (int j = 0; j < fm.getWidth(); ++j) {
                fm.getNeurons()[i][j].setResult(values[i][j]);
            }
        }
    }
}
