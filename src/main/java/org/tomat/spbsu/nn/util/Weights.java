package org.tomat.spbsu.nn.util;

import org.tomat.spbsu.nn.nnet.elements.SharedWeight;

import java.util.Random;
import java.util.function.DoubleSupplier;

public final class Weights {
    public static SharedWeight[][] generateSharedWeights(int width, int height) {
        DoubleSupplier generator = randomSupplier((double)1 / (width * height));
        SharedWeight[][] result = new SharedWeight[height][width];
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                result[i][j] = new SharedWeight(generator);
            }
        }
        return result;
    }

    public static SharedWeight generateBiasWeight(int width, int height) {
        DoubleSupplier generator = randomSupplier(1D / (width * height));
        return new SharedWeight(generator);
    }

    public static DoubleSupplier randomSupplier(double maxAbs) {
        return () -> {
            Random r = new Random();
            double randomValue = -maxAbs + (2 * maxAbs) * r.nextDouble();
            return randomValue;
        };
    }
}
